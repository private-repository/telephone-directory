import React, {useEffect} from "react";
import {StyleSheet, View} from "react-native";
import {DefaultTheme, PaperProvider, Text} from 'react-native-paper';
import Footer from "./src/components/Footer";
import {NavigationContainer} from "@react-navigation/native";
import {createStackNavigator} from "@react-navigation/stack";
import Office from "./src/components/Office";
import Telephone from "./src/components/Telephone";
import * as Font from "expo-font";

const theme = {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: '#273746',
        secondary: '#fff'
    }
};

const Stack = createStackNavigator();

export default function App() {
    const [isFontLoaded, setFontLoaded] = React.useState(false);

    useEffect(() => {
        Font.loadAsync({
            'Poppins-Regular': require('./assets/fonts/Poppins/Poppins-Regular.ttf'),
            'Philosopher-Regular': require('./assets/fonts/Philosopher/Philosopher-Regular.ttf'),
            'Philosopher-Bold': require('./assets/fonts/Philosopher/Philosopher-Bold.ttf')
        }).then(() => {
            setFontLoaded(true);
        }).catch(error => {
            console.error(error);
        })
    }, []);

    if (!isFontLoaded) {
        return (
            <View style={styles.container}>
                <Text style={{color: theme.colors.secondary}}>Loading...</Text>
            </View>
        );
    }

    return (
        <PaperProvider theme={theme}>
            <NavigationContainer>
                <Stack.Navigator screenOptions={{ headerShown: false }}>
                    <Stack.Screen name="Footer" component={Footer} />
                    <Stack.Screen name="Office" component={Office} />
                    <Stack.Screen name="Telephone" component={Telephone} />
                </Stack.Navigator>
            </NavigationContainer>
        </PaperProvider>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0d1117',
        justifyContent: 'center',
        alignItems: 'center'
    }
});
