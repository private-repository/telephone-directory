export default theme = {
    dark: {
        primary: '#0d1117',
        secondary: '#161a21',
        light: '#fff',
        danger: '#ff0000'
    }
}