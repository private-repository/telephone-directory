export default emergency = [
    {
        "id": 1,
        "name": "NEPAL POLICE",
        "phone_no": 100,
        "email": "info@nepalpolice.gov.np",
        "url": "http://www.nepalpolice.gov.np",
        "charge": "FREE",
        "icon": "police-station"
    },
    {
        "id": 2,
        "name": "AMBULANCE",
        "phone_no": 102,
        "email": "nepalambulanceservice@gmail.com",
        "url": "https://www.nepalambulanceservice.org/",
        "charge": "Free",
        "icon": "ambulance"
    },
    {
        "id": 3,
        "name": "FIRE BRIGADE",
        "phone_no": 101,
        "email": "N/A",
        "url": "N/A",
        "charge": "FREE",
        "icon": "fire"
    },
    {
        "id": 4,
        "name": "CHILD HELPLINE (CWIN)",
        "phone_no": 1098,
        "email": "cwin1987@gmail.com",
        "url": "https://cwin.org.np/",
        "charge": "FREE",
        "icon": "baby"
    },
    {
        "id": 5,
        "name": "TORIST POLICE",
        "phone_no": 1144,
        "email": "policetourist@nepalpolice.gov.np",
        "url": "http://www.nepalpolice.gov.np",
        "charge": "FREE",
        "icon": "police-badge"
    },
    {
        "id": 6,
        "name": "CHILD HELPLINE",
        "phone_no": 104,
        "email": "cwin1987@gmail.com",
        "url": "https://cwin.org.np/",
        "charge": "FREE",
        "icon": "baby"
    },
    {
        "id": 7,
        "name": "CIAA (Commission COMMISSION FOR THE INVESTIGATION OF ABUSE OF AUTHORITY)",
        "phone_no": 107,
        "email": "ujuri@ciaa.gov.np",
        "url": "https://narayanilawfirm.org.np/ciaa-nepal/",
        "charge": "FREE",
        "icon": "antenna"
    }
]
