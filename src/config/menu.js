export default menu = [
    {
        name: 'Government',
        subtitle: 'Nepal Government Members Infos',
        icon: 'bank',
        screen: 'Government',
        background: 'blue'
    },
    {
        name: 'Emergency',
        subtitle: 'Nepal Emergency Numbers',
        icon: 'alert',
        screen: 'test',
        background: 'red'
    },
    {
        name: 'Test 1',
        subtitle: 'Subtitle',
        icon: 'home',
        screen: 'test1'
    },
    {
        name: 'Test 2',
        subtitle: 'Subtitle',
        icon: 'home',
        screen: 'test2'
    },
]