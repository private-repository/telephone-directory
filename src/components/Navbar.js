import React from "react";
import {StyleSheet} from "react-native";
import {Appbar } from 'react-native-paper';
import {useNavigation} from "@react-navigation/native";
import colors from "../config/colors";

const Navbar = ({title}) => {
    const navigation = useNavigation();

    return (
        <Appbar.Header  style={styles.container}>
            <Appbar.BackAction color={colors.dark.danger} onPress={() => navigation.goBack()}/>
            <Appbar.Content title={title} color={colors.dark.danger} titleStyle={{fontFamily: 'Philosopher-Bold'}} />
        </Appbar.Header>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: colors.dark.primary,
    }
})

export default Navbar