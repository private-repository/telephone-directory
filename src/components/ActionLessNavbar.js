import React from "react";
import {Appbar, Avatar} from 'react-native-paper';
import colors from "../config/colors";

const ActionLessNavbar = ({title}) => {
    return (
        <Appbar.Header style={{backgroundColor: colors.dark.primary, marginHorizontal: 10}}>
            <Avatar.Image size={60} style = {{backgroundColor: 'transparent'}} source={require('../../assets/everest.png')} />
            <Appbar.Content title={title} titleStyle={{color: colors.dark.danger, fontFamily: 'Philosopher-Bold'}} />
        </Appbar.Header>
    )
}

export default ActionLessNavbar