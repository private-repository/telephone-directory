import React from 'react';
import { View, StyleSheet, FlatList,Linking } from 'react-native';
import {Text,Avatar} from 'react-native-paper';
import colors from "../config/colors";
import AboutList from '../config/about';

const AboutItem = ({ item }) => (
    <>
        <View style={styles.aboutBody}>
            <Text variant="titleMedium" style={{color: colors.dark.light, marginHorizontal: 20, marginTop: 25,fontWeight: 'bold'}}> {item.id}. {item.title}</Text>
            <Text variant="labelSmall" style={{color: colors.dark.light,  marginHorizontal: 20, paddingTop: 5, textAlign: 'justify'}}> {item.info}</Text>
        </View>
    </>
);

const LogoHeader = () => (
    <View style={styles.logo}>
        <Avatar.Image
        size={100}
        style={{ backgroundColor: 'transparent', marginTop: 70, borderRadius: 0 }} source={require('../../assets/everest.png')}/>
        <Text variant="headlineMedium" style={{ color: colors.dark.light, marginTop: 15, fontWeight: 'bold' }}>TEL <Text style={{ color: 'red', fontWeight: 'bold' }}>INFO</Text></Text>
        <Text variant="labelSmall" style={{ color: colors.dark.light, marginTop: 15, alignItems: 'center' }}>Version 1.0.0 - Build</Text>
    </View>
);

const About = () => {

    return (
        <View style={styles.container}>
            <FlatList
                ListHeaderComponent={<LogoHeader />}
                data={about}
                renderItem={({ item }) => <AboutItem item={item} />}
                keyExtractor={(item) => item.id}
                ListFooterComponent={
                    <>
                    <View style={styles.AboutItem} />
                    <Text variant='labelSmall' style={{ color: colors.dark.light, marginHorizontal: 20, marginTop: 20, marginBottom: 20 }}>© 2024 TEL-INFO All rights Reserved.</Text>
                </>
                }
            />
        </View>
    );
};

export default About;

const styles = StyleSheet.create({  
    container: {
        flex: 1,
        backgroundColor: colors.dark.primary,
    },
    logo: {
        alignItems: 'center',
    },
});
