import {FlatList, View, StyleSheet, TouchableOpacity} from "react-native";
import {Card, Text} from "react-native-paper";
import config from "../config/config";
import {useNavigation} from "@react-navigation/native";
import ActionLessNavbar from "./ActionLessNavbar";
import colors from "../config/colors";

const Home = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.container}>
            <ActionLessNavbar title="PROVINCE" />
            <FlatList
                style={{marginTop: 15}}
                data={config.province}
                numColumns={2}
                renderItem={({item}) => (
                    <TouchableOpacity style={{ width: '50%'}} onPress={() => navigation.navigate('Office', { province: item})}>
                        <Card key={item.id} style={styles.card}>
                            <Text variant="titleMedium">{item.prefix.toUpperCase()}</Text>
                            <Text variant="labelSmall"><Text variant="labelSmall" style={{color: '#2980B9'}}>Capital</Text>: {item.capital}</Text>
                            <Text variant="labelSmall"><Text variant="labelSmall" style={{color: '#2980B9'}}>Area</Text>: {item.area}</Text>
                        </Card>
                    </TouchableOpacity>
                )}
                keyExtractor={(item) => item.id.toString()}
            />
        </View>
    );
};

export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.dark.secondary
    },
    card: {
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
        marginHorizontal: 10,
        height: 100,
        justifyContent: 'center',
        borderWidth: 0.5,
        borderColor: '#212F3D',
    },
    cover: {
        borderRadius: 5
    }
})