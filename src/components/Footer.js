import * as React from 'react';
import {BottomNavigation} from 'react-native-paper';
import Home from "./Home";
import Emergency from "./Emergency";
import colors from "../config/colors";
import Hospital from "./Hospital";
import About from "./About";

const HomeRoute = () => <Home />
const EmergencyRoute = () => <Emergency />
const HospitalRoute = () => <Hospital />
const AboutRoute = () => <About />

const Footer = () => {
    const [index, setIndex] = React.useState(0);

    const [routes] = React.useState([
        { key: 'home', title: 'Home', focusedIcon: 'home', unfocusedIcon: 'home-outline'},
        { key: 'emergency', title: 'Emergency', focusedIcon: 'alert', unfocusedIcon: 'alert-outline'},
        { key: 'hospital', title: 'Hospital', focusedIcon: 'hospital-box', unfocusedIcon: 'hospital-box-outline'},
        { key: 'about', title: 'About', focusedIcon: 'account', unfocusedIcon: 'account-outline' }
    ]);

    const renderScene = BottomNavigation.SceneMap({
        home: HomeRoute,
        emergency: EmergencyRoute,
        hospital: HospitalRoute,
        about: AboutRoute
    });

    return (
        <BottomNavigation
            navigationState={{ index, routes }}
            onIndexChange={setIndex}
            renderScene={renderScene}
            barStyle={{ height: 80, backgroundColor: colors.dark.primary }}
            activeColor={colors.dark.danger}
            inactiveColor={colors.dark.light}
            activeIndicatorStyle={{backgroundColor: colors.dark.primary}}
    />
    );
};

export default Footer;