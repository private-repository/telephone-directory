import {FlatList, TouchableOpacity, View, StyleSheet} from "react-native";
import {Card, Text} from 'react-native-paper';
import config from "../config/config";
import {useNavigation, useRoute} from "@react-navigation/native";
import Navbar from "./Navbar";
import colors from "../config/colors";

const Office = () => {
    const navigation = useNavigation();
    const route = useRoute();
    const { province } = route.params;
    const office_types = config.office_types;

    return (
        <View style={styles.container}>
            <Navbar title="DEPARTMENT" />
            <View style={{ paddingHorizontal: 16, marginTop: 15 }}>
                <FlatList
                    data={office_types}
                    renderItem={({item}) => (
                        <TouchableOpacity onPress={() => navigation.navigate('Telephone', { province: province, office_type: item })}>
                            <Card style={styles.card}>
                                <Card.Content>
                                    <Text variant="titleMedium" style={{color: colors.dark.light}}>{item.name}</Text>
                                </Card.Content>
                            </Card>
                        </TouchableOpacity>
                    )}
                    keyExtractor={(item) => item.id.toString()}
                    showsVerticalScrollIndicator={false}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.dark.secondary
    },
    card: {
        marginVertical: 5,
        marginHorizontal: 5,
        borderRadius: 5,
        backgroundColor: colors.dark.primary
    }
});

export default Office;