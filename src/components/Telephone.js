import {useEffect, useState} from 'react';
import {FlatList, View, StyleSheet, ActivityIndicator, RefreshControl} from "react-native";
import {Card, Searchbar, Text} from "react-native-paper";
import {useRoute} from "@react-navigation/native";
import Navbar from "./Navbar";
import {Dropdown} from "react-native-element-dropdown";
import data from "../data/government.json";
import colors from "../config/colors";

const Telephone = () => {
    const route = useRoute();
    const { province, office_type } = route.params;

    const [offices, setOffices] = useState([]);
    const [members, setMembers] = useState([]);
    const [filteredMembers, setFilteredMembers] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [placeholder, setPlaceholder] = useState('All');
    const [total, setTotal] = useState(0);

    const [isLoading, setIsLoading] = useState(true);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = (office_id = null) => {
        setRefreshing(true);

        let backup = [];
        let result = data[province.prefix].filter(member => office_type.prefix.includes(member.office.name.split(',')[0].trim()));
        backup = result;

        if (office_id) {
            result = backup.filter(member => member.office_id === office_id);
        }

        setTotal(result.length);

        let uniqueIds = new Set();
        const selectList = [{id: 0, name: 'All'}];

        backup.forEach(member => {
            if (!uniqueIds.has(member.office_id)) {
                uniqueIds.add(member.office_id);
                selectList.push({ id: member.office_id, name: member.office.office_name });
            }
        });

        setOffices(selectList);

        setTimeout(() => {
            setMembers(result);
            setFilteredMembers(result);
            setRefreshing(false);
            setIsLoading(false);
        }, 500);
    }

    const handleMemberSearch = (text) => {
        setSearchQuery(text);
        const result = members.filter(member =>
            member.name.toLowerCase().includes(text.toLowerCase())
        );
        setFilteredMembers(result);
        setTotal(result.length);
    };

    const renderMembers = ({item}) => (
        <Card key={item.member_id} style={styles.card}>
            <Text variant="labelLarge" style={{color: colors.dark.light}}><Text variant="labelLarge" style={{color: '#7FB3D5'}}>Name</Text>: {item.name}</Text>
            <Text variant="labelLarge" style={{color: colors.dark.light}}><Text variant="labelLarge" style={{color: '#7FB3D5'}}>Designation</Text>: {item.designation?.title}</Text>
            <Text variant="labelLarge" style={{color: colors.dark.light}}><Text variant="labelLarge" style={{color: '#7FB3D5'}}>Service Started</Text>: {item.member.service_start_date ?? 'N/A'}</Text>
            <Text variant="labelLarge" style={{color: colors.dark.light}}><Text variant="labelLarge" style={{color: '#7FB3D5'}}>Telephone</Text>: {item.office_phone ?? 'N/A'}</Text>
            <Text variant="labelLarge" style={{color: colors.dark.light}}><Text variant="labelLarge" style={{color: '#7FB3D5'}}>Mobile</Text>: {item.mobile ?? 'N/A'}</Text>
            <Text variant="labelLarge" style={{color: colors.dark.light}}><Text variant="labelLarge" style={{color: '#7FB3D5'}}>Email</Text>: {item.email ?? 'N/A'}</Text>
            <Text variant="labelLarge" style={{color: colors.dark.light}}><Text variant="labelLarge" style={{color: '#7FB3D5'}}>Office</Text>: {item.office.name ?? 'N/A'}</Text>
        </Card>
    );

    const renderItem = item => {
        return (
            <View style={styles.item}>
                <Text variant="labelLarge" style={{fontWeight: 'normal', color: colors.dark.light}}>{item.name}</Text>
            </View>
        );
    };

    return (
        <View style={styles.container}>
            <Navbar title="INFORMATION"/>
            <Searchbar
                style={styles.search}
                placeholder="Search"
                onChangeText={handleMemberSearch}
                value={searchQuery}
                placeholderTextColor="gray"
                inputStyle={{ color: colors.dark.light}}
            />
            <Dropdown
                style={styles.dropdown}
                placeholderStyle={styles.placeholderStyle}
                containerStyle={{borderWidth: 0, borderRadius: 5, marginTop: 5, backgroundColor: colors.dark.secondary, shadowColor: '#fff', shadowRadius: 1}}
                data={offices}
                maxHeight={300}
                labelField="name"
                valueField="id"
                placeholder={placeholder}
                onChange={(office) => {
                    setPlaceholder(office.name);
                    getData(office.id);
                }}
                renderItem={renderItem}
            />
            { isLoading ?
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator animating={true} color="white"/>
                </View>
                :
                <>
                    <Text variant="labelSmall" style={{textAlign: 'right', marginEnd: 10, marginTop: 10, color: colors.dark.light}}>Total: {total}</Text>
                    { filteredMembers.length?
                        <FlatList
                            style={{ marginTop: 5}}
                            data={filteredMembers}
                            renderItem={renderMembers}
                            keyExtractor={(item) => item.member_id.toString()}
                            showsVerticalScrollIndicator={false}
                            refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                    onRefresh={getData}
                                    colors={['white']}
                                    tintColor={'white'}
                                />
                            }
                        />

                        :
                        <Text style={styles.notfound}>Cant find any member. Please Modify your search and try again.</Text>
                    }
                </>
            }

        </View>
    );
};

export default Telephone;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.dark.secondary
    },
    card: {
        marginHorizontal: 10,
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
        backgroundColor: colors.dark.primary
    },
    search: {
        marginVertical: 10,
        marginHorizontal: 10,
        borderRadius: 5,
        backgroundColor: colors.dark.primary,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
    },
    dropdown: {
        marginHorizontal: 10,
        height: 55,
        backgroundColor: colors.dark.primary,
        padding: 12,
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
    },
    item: {
        paddingHorizontal: 17,
        paddingVertical: 10
    },
    placeholderStyle: {
        color: colors.dark.light
    },
    notfound: {
        marginHorizontal: 15,
        marginTop: 10
    }
})