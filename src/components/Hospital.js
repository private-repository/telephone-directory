import {ActivityIndicator, FlatList, RefreshControl, StyleSheet, View} from "react-native";
import {Card, Searchbar, SegmentedButtons, Text} from "react-native-paper";
import colors from "../config/colors";
import {Dropdown} from "react-native-element-dropdown";
import {useEffect, useState} from "react";
import ActionLessNavbar from "./ActionLessNavbar";
import config from "../config/config";

const type = {
    all: 1,
    province: 2,
    disease: 3
}

const Hospital = () => {
    const [hospitals, setHospitals] = useState([]);
    const [filteredHospitals, setFilteredHospitals] = useState([]);
    const [searchQuery, setSearchQuery] = useState('');
    const [segment, setSegment] = useState(type.all);
    const [diseases, setDiseases] = useState([
        {name: 'All', value: ''},
        {name: 'Heart', value: 'heart'},
        {name: 'Cancer', value: 'cancer'},
        {name: 'Kidney', value: 'kidney'},
        {name: 'Parkinson', value: 'parkinson'},
        {name: 'Alzheimer', value: 'alzheimer'},
        {name: 'Head Injuries', value: 'head_injuries'},
        {name: 'Spinal Injuries', value: 'spinal_injuries'},
        {name: 'Sickle Cell Anemia', value: 'sickle_cell_anemia'}
    ]);
    const [provinces, setProvinces] = useState([{name: 'All', prefix: ''}, ...config.province]);
    const [diseasePlaceholder, setDiseasePlaceholder] = useState({name: 'All', value: ''});
    const [provincePlaceholder, setProvincePlaceholder] = useState({name: 'All', prefix: ''});
    const [total, setTotal] = useState(0);

    const [isLoading, setIsLoading] = useState(true);
    const [refreshing, setRefreshing] = useState(false);

    useEffect(() => {
        getData();
    }, []);

    const getData = (timeout = true) => {
        const diseases = config.diseases;
        const provinces = config.hospitals;
        const hospitals = Array.from(
            new Set([
                ...Object.values(diseases).flat(),
                ...Object.values(provinces).flat(),
            ])
        );

        setTotal(hospitals.length);

        if (timeout) {
            setTimeout(() => {
                setHospitals(hospitals);
                setFilteredHospitals(hospitals);
                setRefreshing(false);
                setIsLoading(false);
            }, 500);
        } else {
            setHospitals(hospitals);
            setFilteredHospitals(hospitals);
            setRefreshing(false);
            setIsLoading(false);
        }

    }

    const handleHospitalSearch = (text) => {
        setSearchQuery(text);
        const result = hospitals.filter(hospital =>
            hospital.toLowerCase().includes(text.toLowerCase())
        );
        setFilteredHospitals(result);
        setTotal(result.length);
    };

    const renderHospitals = ({item}) => (
        <Card key={item} style={styles.card}>
            <Text variant="labelLarge" style={{color: colors.dark.light}}>{item}</Text>
        </Card>
    );

    const renderItem = item => {
        return (
            <View style={styles.item}>
                <Text variant="labelLarge" style={{fontWeight: 'normal', color: colors.dark.light}}>{item.name}</Text>
            </View>
        );
    };

    const setProvinceHospitals = (section = '') => {
        let provinceHospitals = [];
        if (!section) {
            provinceHospitals = Array.from(new Set(Object.values(config.hospitals).flat()));
        } else {
            provinceHospitals = config.hospitals[section];
        }
        setHospitals(provinceHospitals);
        setFilteredHospitals(provinceHospitals);
        setTotal(provinceHospitals.length);
        setRefreshing(false);
    }

    const setDiseaseHospitals = (section = '') => {
        let diseaseHospitals = [];
        if (!section) {
            diseaseHospitals = Array.from(new Set(Object.values(config.diseases).flat()));
        } else {
            diseaseHospitals = config.diseases[section];
        }
        setHospitals(diseaseHospitals);
        setFilteredHospitals(diseaseHospitals);
        setTotal(diseaseHospitals.length);
        setRefreshing(false);
    }
    
    return (
        <View style={styles.container}>
            <ActionLessNavbar title="HOSPITAL"/>
            <SegmentedButtons
                style={{marginHorizontal: 10, marginTop: 10}}
                value={segment}
                onValueChange={(value) => {
                    setSegment(value);
                    switch (value) {
                        case type.province:
                            setProvinceHospitals(provincePlaceholder.prefix);
                            break;
                        case type.disease:
                            setDiseaseHospitals(diseasePlaceholder.value);
                            break;
                        default:
                            getData(false);
                            break;
                    }
                }}
                buttons={[
                    {label: 'All', value: 1, uncheckedColor: colors.dark.light, style: {borderRadius: 5}},
                    {label: 'Province', value: 2, uncheckedColor: colors.dark.light},
                    {label: 'Disease', value: 3, uncheckedColor: colors.dark.light, style: {borderRadius: 5}}
                ]}
            />
            <Searchbar
                style={styles.search}
                placeholder="Search"
                onChangeText={handleHospitalSearch}
                value={searchQuery}
                placeholderTextColor="gray"
                inputStyle={{ color: colors.dark.light}}
            />
            {
                (segment !== type.all)?
                    <>
                        {
                            (segment === type.province)?
                                <Dropdown
                                    style={styles.dropdown}
                                    placeholderStyle={styles.placeholderStyle}
                                    selectedTextStyle={{ color: colors.dark.light}}
                                    containerStyle={{borderWidth: 0, borderRadius: 5, marginTop: 5, backgroundColor: colors.dark.secondary, shadowColor: '#fff', shadowRadius: 1}}
                                    data={provinces}
                                    maxHeight={300}
                                    labelField="name"
                                    valueField="prefix"
                                    placeholder={provincePlaceholder.name}
                                    onChange={(item) => {
                                        setProvincePlaceholder(item);
                                        setProvinceHospitals(item.prefix);
                                    }}
                                    renderItem={renderItem}
                                />:
                                <Dropdown
                                    style={styles.dropdown}
                                    placeholderStyle={styles.placeholderStyle}
                                    selectedTextStyle={{ color: colors.dark.light}}
                                    containerStyle={{borderWidth: 0, borderRadius: 5, marginTop: 5, backgroundColor: colors.dark.secondary, shadowColor: '#fff', shadowRadius: 1}}
                                    data={diseases}
                                    maxHeight={300}
                                    labelField="name"
                                    valueField="value"
                                    placeholder={diseasePlaceholder.name}
                                    onChange={(item) => {
                                        setDiseasePlaceholder(item);
                                        setDiseaseHospitals(item.value);
                                    }}
                                    renderItem={renderItem}
                                />
                        }
                    </>: null
            }
            { isLoading ?
                <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <ActivityIndicator animating={true} color="white"/>
                </View>
                :
                <>
                    <Text variant="labelSmall" style={{textAlign: 'right', marginEnd: 10, marginTop: 10, color: colors.dark.light}}>Total: {total}</Text>
                    { filteredHospitals.length?
                        <FlatList
                            style={{ marginTop: 5}}
                            data={filteredHospitals}
                            renderItem={renderHospitals}
                            keyExtractor={(item) => item.toString()}
                            showsVerticalScrollIndicator={false}
                            refreshControl={
                                <RefreshControl
                                    refreshing={refreshing}
                                    onRefresh={() => {
                                        setRefreshing(true);
                                        switch (segment) {
                                            case type.province:
                                                setProvinceHospitals(provincePlaceholder.prefix);
                                                break;
                                            case type.disease:
                                                setDiseaseHospitals(diseasePlaceholder.value);
                                                break;
                                            default:
                                                getData();
                                                break;
                                        }
                                    }}
                                    colors={['white']}
                                    tintColor={'white'}
                                />
                            }
                        />

                        :
                        <Text style={styles.notfound}>Cant find any hospital. Please Modify your search and try again.</Text>
                    }
                </>
            }

        </View>
    );
};

export default Hospital;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.dark.secondary
    },
    card: {
        marginHorizontal: 10,
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
        backgroundColor: colors.dark.primary
    },
    search: {
        marginTop: 10,
        marginHorizontal: 10,
        borderRadius: 5,
        backgroundColor: colors.dark.primary,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
    },
    dropdown: {
        marginTop: 5,
        marginHorizontal: 10,
        height: 55,
        backgroundColor: colors.dark.primary,
        padding: 12,
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.2,
        shadowRadius: 1.41,
        elevation: 2,
    },
    item: {
        paddingHorizontal: 17,
        paddingVertical: 10,
    },
    placeholderStyle: {
        color: colors.dark.light
    },
    notfound: {
        marginHorizontal: 15,
        marginTop: 10
    }
});