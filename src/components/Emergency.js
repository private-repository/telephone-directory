import React from 'react';
import { View, StyleSheet, FlatList,Linking } from 'react-native';
import {List, Card, Text} from 'react-native-paper';
import EmergencyList from '../config/emergency';
import ActionLessNavbar from "./ActionLessNavbar";
import colors from "../config/colors";

const EmergencyItem = ({ item }) => (
    <Card  style={styles.card}>
        <List.Accordion title={item.name} titleStyle={{ color: colors.dark.light }} rippleColor={colors.dark.primary} style={{ backgroundColor: colors.dark.primary }} 
        id={item.id} 
        left={(props) => <List.Icon {...props} icon={item.icon} color="#5174A0"/>}>
            <Text variant="labelLarge" style={{ color: colors.dark.light }}>PHONE NO : {item.phone_no}</Text>
            <Text variant="labelLarge" style={{ color: colors.dark.light }}>E-MAIL : {item.email}</Text>
            <Text variant="labelLarge" style={{ color: colors.dark.light }}>CHARGE : {item.charge}</Text>
            <Text variant="labelMedium" style={styles.link} onPress={() => Linking.openURL(item.url)}>{item.url}</Text>
        </List.Accordion>
    </Card>
);

const Emergency = () => {
    return (
        <View style={styles.container}>
            <ActionLessNavbar title="EMERGENCY" />
            <FlatList
                data={EmergencyList}
                renderItem={({ item }) => <EmergencyItem item={item} />}
                keyExtractor={(item) => item.id}
                ListFooterComponent={<View style={styles.emergencyItems} />}
            />
        </View>
    );
};

export default Emergency;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.dark.secondary
    },
    header: {
        justifyContent: 'center',
        alignItems: 'center',
    },

    card: {
        padding: 10,
        borderRadius: 5,
        marginTop: 10,
        marginHorizontal: 10,
        backgroundColor: colors.dark.primary
    },

    link: {
        color: '#3498DB',
        textDecorationLine: 'underline',
        marginVertical: 10,
    },
    emergencyItems: {
        marginBottom: 10,
    },

});
